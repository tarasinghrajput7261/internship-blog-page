    // Add functionality to display and hide the dropdown menus
    const dropdowns = document.querySelectorAll('.dropdown');
    const subMenu = document.querySelectorAll('.dropdown-submenu');
    
    dropdowns.forEach((dropdown) => {
      dropdown.addEventListener('mouseover', () => {
        dropdown.querySelector('.dropdown-menu').style.display = 'flex';
    
        subMenu.forEach((subMenuDropdown) => {
          subMenuDropdown.addEventListener('mouseover', () => {
            subMenuDropdown.querySelector('.sub-segments').style.display = 'flex';
          });
    
          subMenuDropdown.addEventListener('mouseout', () => {
            subMenuDropdown.querySelector('.sub-segments').style.display = 'none';
          });
        })
      });
    
      dropdown.addEventListener('mouseout', () => {
        dropdown.querySelector('.dropdown-menu').style.display = 'none';
      });
    });
    

    function toggleNavMenu() {
      var navMenu = document.getElementById('nav-menu');
      navMenu.classList.toggle('active');
    }
    
    
    // slider code
    const slider = document.querySelector('.slider');
    const slides = document.querySelectorAll('.slide');
    const prevBtn = document.querySelector('.prev');
    const nextBtn = document.querySelector('.next');
    
    let slideIndex = 0;
    
    prevBtn.addEventListener('click', () => {
      slideIndex = (slideIndex - 1 + slides.length) % slides.length;
      updateSliderPosition();
    });
    
    nextBtn.addEventListener('click', () => {
      slideIndex = (slideIndex + 1) % slides.length;
      updateSliderPosition();
    });
    
    function updateSliderPosition() {
      const slideWidth = slides[0].offsetWidth;
      slider.style.transform = `translateX(-${slideIndex * slideWidth}px)`;
    }
    
    window.addEventListener('resize', updateSliderPosition);
    
    updateSliderPosition();
    